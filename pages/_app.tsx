import type { AppProps } from 'next/app'
import "bootstrap/dist/css/bootstrap.min.css"
import "../src/styles/app.scss"
import BaseLayout from '../src/components/common/BaseLayout'

export default function App({ Component, pageProps }: AppProps) {
  return (
    <BaseLayout>
      <Component {...pageProps} />
    </BaseLayout>
  )
}
