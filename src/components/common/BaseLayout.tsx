import { Fragment } from "react"
import Footer from "./Footer"
import Header from "./Header"
import HeaderTop from "./HeaderTop"


const BaseLayout = ({ children }: any) => {
    return(
        <Fragment>
            <HeaderTop/>
            <Header />
            <div>
                {children}
            </div>
            <Footer />
        </Fragment>
    )
}

export default BaseLayout