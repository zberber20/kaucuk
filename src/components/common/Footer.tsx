import { Facebook, Instagram, Linkedin, Phone, Twitter } from "react-feather"


const Footer = () => {
    return(
        <footer className="w-100">
            <div className="container">
                <div className="row">
                    <div className="col-md-4 col-sm-12 d-flex justify-content-center align-items-center">
                        <span style={{fontSize: "40px"}}>M&H Kavuçuk</span>
                    </div>
                    <div className="col-md-4 col-sm-12 d-flex justify-content-around align-items-center my-4">
                        <Twitter size={25} /> <Facebook size={25} /> <Instagram size={25} /> <Linkedin size={25} />
                    </div>
                    <div className="col-md-4 col-sm-12 d-flex justify-content-center align-items-center mb-5">
                        <Phone size={25} /> <span className="mx-3" style={{fontSize: "25px"}}>0555-555-55-55</span>
                    </div>
                </div>
                <div className="row my-3">
                    <div className="border border-top"></div>
                </div>
                <div className="row">
                    <div className="w-100 d-flex justify-content-center">made by&nbsp;<strong style={{color: "#439A97"}}>zuberman</strong>&nbsp;with&nbsp;<span className="text-danger">❤️</span> </div>
                </div>
            </div>
        </footer>
    )
}

export default Footer