import Link from "next/link"
import { Fragment, useEffect, useState } from "react"
import { Container, Nav, Navbar, NavDropdown } from "react-bootstrap"
import { Menu, X } from "react-feather"


const Header = () => {
    const size = useWindowSize()
    return (
        <Fragment>
            <div className="navbar w-100 d-flex justify-content-center">
                <div className="row w-100">
                    <Navbar expand="lg" className="d-flex justify-content-between">
                        <Container className="d-flex justify-content-between">
                            <Navbar.Brand style={{fontSize: "35px"}}>M&H Kavuçuk</Navbar.Brand>
                            <Navbar.Toggle aria-controls="basic-navbar-nav" />
                            <Navbar.Collapse id="basic-navbar-nav">
                                <Nav className="me-auto">
                                    <Nav.Link>Home</Nav.Link>
                                    <Nav.Link>Institutional</Nav.Link>
                                    <Nav.Link>Products</Nav.Link>
                                    <Nav.Link>Certificates</Nav.Link>
                                    <Nav.Link>Contact</Nav.Link>
                                </Nav>
                            </Navbar.Collapse>
                        </Container>
                    </Navbar>
                </div>
                {/* <nav className="container py-3 py-md-4 w-100">
                    <div className="row w-100 navbar__row" >
                        <div className="col-6 col-md-4 d-flex align-items-center"><h1>LOGO</h1></div>
                        {size.width > 991 ?
                            <div className="col-6 col-md-8 d-flex align-items-center justify-content-end gap-4 navbar__nav-menu">
                                <Link href={''}>HOME</Link>
                                <Link href={''}>INSTITUTIONAL</Link>
                                <Link href={''}>PRODUCTS</Link>
                                <Link href={''}>CERTIFICATES</Link>
                                <Link href={''}>CONTACT</Link>
                            </div> :
                            <div className="col-6 col-md-8 d-flex align-items-center justify-content-end">
                                <Menu />
                            </div>
                        }
                    </div>
                </nav> */}
            </div>
        </Fragment>
    )
}

export default Header

function useWindowSize() {
    const [windowSize, setWindowSize] = useState({
        width: 0,
        height: 0,
    });

    useEffect(() => {
        if (typeof window !== 'undefined') {
            const handleResize = () => {
                setWindowSize({
                    width: window.innerWidth,
                    height: window.innerHeight,
                })
            }
            window.addEventListener("resize", handleResize)
            handleResize();
            return () => window.removeEventListener("resize", handleResize)
        }
    }, [])
    return windowSize
}
