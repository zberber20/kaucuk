import { Clock, Facebook, Instagram, Linkedin, MapPin, Phone, Twitter } from 'react-feather';

const HeaderTop = () => {
    return(
        <div className="d-flex justify-content-center w-100 header-top__wrapper">
            <div className="container">
                <div className="row">
                    <div className="col-12 col-md-4 d-flex justify-content-center justify-content-md-start align-items-center my-2">
                        <MapPin size={15} />{" "} <span className='ml-2'> Şirket Adresi Apt. No.10 Erdoğdu, Trabzon</span>
                    </div>
                    <div className="col-12 col-md-4 d-none d-md-flex justify-content-around align-items-center my-2">
                        <Twitter size={15} /> <Facebook size={15} /> <Instagram size={15} /> <Linkedin size={15} />
                    </div>
                    <div className="col-12 col-md-4 d-flex justify-content-center justify-content-md-end align-items-center my-2">
                        <div>
                            <Phone size={15} /> <span className="ml-2">0555-555-55-55</span>
                        </div>
                        <div className="h-75 mx-3 header-middle-divider"></div>
                        <div>
                            <Clock size={15} /> <span className="ml-2">7 / 24</span>
                        </div>
                        <div className="h-75 mx-3 header-middle-divider"></div>
                        <div className='d-flex'>
                            <img src="/images/tr.svg" alt="tr-flag" className='mx-1 rounded' width={37} height={25} />
                            <img src="/images/gb.svg" alt="gb-flag" className='mx-1 rounded' width={37} height={25} />
                        </div>

                    </div>
                </div>
                <div className="row">
                    <div className="col-12 header-divider"></div>
                </div>
            </div>
        </div>
    )
}

export default HeaderTop