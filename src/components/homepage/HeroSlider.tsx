import Carousel from 'react-bootstrap/Carousel';

const HeroSlider = () => {
    return (
        <section className='main-carousel w-100'>
            <Carousel>
                <Carousel.Item>
                    <div className='carousel-background-one'>
                        <div className="dark-curtain d-flex align-items-center justify-content-center">
                            <div className='dark-curtain__text-wrapper text-center'>
                                <img src="https://fennerdunlopamericas.com/app/uploads/impact_photo_dunlop_conveyor_belting_drachten_035-1.jpg" alt="" />
                                <Carousel.Caption>
                                    <h2>Lorem ipsum dolor sit.</h2>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Alias nesciunt nobis nisi soluta repellat.</p>
                                </Carousel.Caption>
                            </div>
                        </div>
                    </div>
                </Carousel.Item>
                <Carousel.Item>
                    <div className='carousel-background-two'>
                        <div className="dark-curtain d-flex align-items-center justify-content-center">
                            <div className='dark-curtain__text-wrapper text-center'>
                                <img src="https://www.giz.de/static/en/media/20221019_Auftraggeber_1920x700.jpg" alt="" />
                                <Carousel.Caption>
                                    <h2>Lorem ipsum dolor sit amet, consectetur </h2>
                                    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Voluptate, vitae!</p>
                                </Carousel.Caption>
                            </div>
                        </div>
                    </div>
                </Carousel.Item>
            </Carousel>
        </section>
    )
}

export default HeroSlider