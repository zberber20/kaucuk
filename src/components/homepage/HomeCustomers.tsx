

const HomeCustomers = ( ) => {
    return(
        <section className="testimonial-section my-5 ">
            <div className="container">
                <div className="row d-flex justify-content-center">
                    <div className="col-md-7 col-sm-12">
                        <div className="card p-5 my-5 contact-card">
                            <div className="row">
                                <div className="col-md-6 col-lg-6">
                                    <form className="contact-form">
                                        <div className="form-group mb-4">
                                            <label htmlFor="">First Name & Last Name</label>
                                            <input type="text" className="form-control" />
                                        </div>
                                        <div className="form-group mb-4">
                                            <label htmlFor="">Phone</label>
                                            <input type="text" className="form-control" />
                                        </div>
                                        <div className="form-group mb-4">
                                            <label htmlFor="">Email</label>
                                            <input type="text" className="form-control" />
                                        </div>
                                        <div className="form-group mb-4">
                                            <label htmlFor="topic">Enter Your Message</label>
                                            <textarea name="topic" id="topic" cols={30} rows={3} className="form-control"></textarea>
                                        </div>
                                        <div className="form-group text-center">
                                            <button className="btn">Send Message</button>
                                        </div>
                                    </form>
                                </div>
                                <div className="d-none d-md-flex col-6">
                                    <img src="/images/businesswoman.png" className="contact-image" alt="contact-image" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default HomeCustomers