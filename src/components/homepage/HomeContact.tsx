

const HomeContact = () => {
    return(
        <section className="contact-section">
            <div className="container mb-5">
                <div className="row">
                    <div className="col-sm-12 col-md-6 mb-4 mb-md-0 d-flex justify-content-center justify-content-md-end">
                        <img src="/images/personalphoto-removebg-preview.png" alt="mustafa-berber" height={400} />
                    </div>
                    <div className="col-sm-12 col-md-6 d-flex justify-content-center justify-content-md-start align-items-center">
                        <div className="text-center col-sm-12 col-md-8">
                            <h2 className="contact-section__person-namemb-3">MUSTAFA BERBER</h2>
                            <h4 className="mb-2">Better call Mustafa...</h4>
                            <h4 className="text-dark">Lorem ipsum dolor sit amet consectetur adipisicing elit. Rerum nemo adipisci velit aperiam modi corporis unde, iure totam. Eligendi aliquid aliquam minima neque non consequatur dolor, magni pariatur similique accusamus.</h4>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default HomeContact