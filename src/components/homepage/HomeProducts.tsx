import { Carousel } from "react-bootstrap"


const HomeProducts = () => {
    return(
        <section className="home-products py-3 my-5">
            <Carousel>
                <Carousel.Item>
                    <div className="d-flex justify-content-around">
                        <img src="/images/bottom-sliders/images.jpeg" alt="image" className="little-slider-image" />
                        <img src="/images/bottom-sliders/Molded-Rubber-Products-CR-Rubber-Sheeting.jpeg" alt="image" className="little-slider-image" />
                        <img src="/images/bottom-sliders/rubber-fabric-5fa1aaae4909c61a1ef737409ddc946a.jpeg" alt="image" className="little-slider-image" />
                    </div>
                </Carousel.Item>
                <Carousel.Item>
                    <div className="d-flex justify-content-around">
                        <img src="/images/bottom-sliders/rubber-industry-urges-government-to-correct-inverted-duty-structure.webp" alt="image" className="little-slider-image" />
                        <img src="/images/bottom-sliders/rubber-sheet-rolls.jpeg" alt="image" className="little-slider-image" />
                        <img src="/images/bottom-sliders/rubber-tyre.jpeg" alt="image" className="little-slider-image" />
                    </div>
                </Carousel.Item>
            </Carousel>
        </section>
    )
}

export default HomeProducts