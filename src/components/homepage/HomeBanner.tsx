

const HomeBanner = () => {
    return(
        <section className="banner-section">
            <div className="banner-section__inner-wrapper d-flex align-items-center">
                <div className="row w-100">
                    <div className="col-sm-12 col-md-4 d-flex justify-content-center">
                        <div className="text-center">
                            <h2>+100</h2>
                            <h4>client</h4>
                        </div>
                    </div>
                    <div className="col-sm-12 col-md-4 d-flex justify-content-center">
                        <div className="text-center">
                            <h2>13</h2>
                            <h4>country</h4>
                        </div>
                    </div>
                    <div className="col-sm-12 col-md-4 d-flex justify-content-center">
                        <div className="text-center">
                            <h2>+10.000</h2>
                            <h4>happy people</h4>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default HomeBanner