

const HomeCards = () => {
    return(
        <section className="home-card-section d-flex justify-content-around">
            <div className="container">
                <div className="row">
                    <div className="card-wrapper col-sm-12 col-md-4">
                        <div className="card pt-5 pb-4 m-2">
                            <img src="/images/undraw_connected_world_wuay (1).svg" alt="global" height={150} />
                            <div className="card-home-texts p-1 p-md-3 text-center mt-3">
                                <h2>All around the world</h2>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia sint sed facere atque, doloribus dolor neque eum, illo tempora saepe iure magnam similique aut. Illum omnis minima culpa aliquam rerum?</p>
                            </div>
                        </div>
                    </div>
                    <div className="card-wrapper col-sm-12 col-md-4">
                        <div className="card pt-5 pb-4 m-2">
                        <img src="/images/undraw_business_deal_re_up4u (1).svg" alt="deal" height={150} />
                            <div className="card-home-texts p-1 p-md-3 text-center mt-3">
                                <h2>Best deals with win-win strategy</h2>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia sint sed facere atque, doloribus dolor neque eum, illo tempora saepe iure magnam similique aut. Illum omnis minima culpa aliquam rerum?</p>
                            </div>
                        </div>
                    </div>
                    <div className="card-wrapper col-sm-12 col-md-4">
                        <div className="card pt-5 pb-4 m-2">
                            <img src="/images/undraw_connecting_teams_re_hno7 (1).svg" alt="connect" height={150} />
                            <div className="card-home-texts p-1 p-md-3 text-center mt-3">
                                <h2>Every where every time</h2>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia sint sed facere atque, doloribus dolor neque eum, illo tempora saepe iure magnam similique aut. Illum omnis minima culpa aliquam rerum?</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    )
}

export default HomeCards